// modules and dependencies
var router = require('express')()
var server = require('http').Server(router)
var bodyParser = require('body-parser')
var mongoose = require('mongoose')

// database configuration
var userModel = require('./db/models/user.js')
var dbConfig = require('./db/config.js')

// controllers
var root = require('./controllers/index.js')
var users = require('./controllers/users.js')

// mongoose connection
mongoose.connect(
  'mongodb://' + dbConfig.user + ':' + dbConfig.pass + '@' + dbConfig.host +
  ':' + dbConfig.port + '/' + dbConfig.database
)

// router config
router.use(bodyParser.json())
router.use(bodyParser.urlencoded({ extended: true }))

// template config
router.set('view engine', 'jade')
router.set('views', __dirname + '/public')

// static files
router.use(require('express').static('public'))

// Routing
router.get('/', root.index)
router.get('/users', users.get.index)
router.post('/users', users.post.index)

server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function() {
  var addr = server.address()
  console.log("Chat server listening at", addr.address + ":" + addr.port)
})
