var userModel = require('../db/models/user.js')

var user = {}

user.get = {
    index: function (req, res) {
        res.send('Hello')
    }
}

user.post = {
    index: function (req, res) {
        var user = new userModel({
            username: req.body.username,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
        })
        
        user.save()
          
        res.render('users', {
            msg: 'User created successfully',
            user: user,
            created: true
        })
    }
}

module.exports = exports = user