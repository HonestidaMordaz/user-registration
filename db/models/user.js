var mongoose = require('mongoose')
var userSchema = require('../schemas/user.js')

var UserModel = mongoose.model('users', userSchema);

module.exports = exports = UserModel