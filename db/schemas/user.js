var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
  username: String,
  firstName: String,
  lastName: String,
  email: String,
  date: { type: Date, default: Date.now }
});

module.exports = exports = userSchema